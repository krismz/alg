# README #

This repository contains code implementing Aggregation of Longitudinal Geodesics (ALG) as described
in

"Nonparametric Aggregation of Geodesic Trends for Longitudinal Data Analysis".
Kristen M. Campbell and P. Thomas Fletcher,
International Workshop on Shape in Medical Imaging (ShapeMI) at MICCAI, 2018.
In: Reuter M., Wachinger C., Lombaert H., Paniagua B., Luthi M., Egger B. (eds) Shape in Medical Imaging.
ShapeMI 2018. Lecture Notes in Computer Science, vol 11167. Springer, Cham, 2018. [10.1007/978-3-030-04747-4_22](https://doi.org/10.1007/978-3-030-04747-4_22)


This code is heavily reliant on Nikhil Singh's code in
[VectorMomentum](https://bitbucket.org/scicompanat/vectormomentum/) for preparing the 3D brain image geodesics and
atlases analyzed here.

## Documentation In Progress ##
The documentation in this repository is under development.  In the meantime, if you have questions
about ALG code, please contact Kris Campbell (kris at sci.utah.edu).



### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
