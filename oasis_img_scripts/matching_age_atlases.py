#!/usr/bin/python2
import sys
import os.path
from optparse import OptionParser
import csv

# pyca
import PyCA.Core as ca
import PyCA.Common as common

# vector momentum configs
from Configs import Config

# vector momentum modules
from Applications import CAvmMatching as vmMatching

def subj_details(ages, indir, cfg):
  # TODO this is very specific to my ageatlas experiment, change to config params instead?
  subj_id = "atlas"
  imgs = []
  for a in ages:
    imgs.append(os.path.join(indir, "{0}_age_{1}.mhd".format(subj_id, a)))
  
  return (subj_id, imgs)  


def do_matching(ages, times, indir, outdir):
# find momenta at each age by image matching from one age atlas to the next
 
  # setup config
  match_cfg = Config.SpecToConfig(vmMatching.MatchingConfigSpec)

  match_cfg.compute.useCUDA = False # True?
  match_cfg.compute.interactive = False # True?
  match_cfg.optim.nTimeSteps = 10
  match_cfg.optim.Niter = 30 # Nikhil used 2000, but convergence seems to happen around 30
  match_cfg.optim.stepSize = 0.000002
  match_cfg.optim.integMethod = "RK4"
  match_cfg.vectormomentum.sigma = 0.1
  match_cfg.io.plotEvery = -1


  for ageidx in range(len(ages)-1):
    match_cfg.study.I0 = os.path.join(indir, 
                                      "atlas_age_{0}.mhd".format(ages[ageidx]))
    match_cfg.study.I1 = os.path.join(indir, 
                                      "atlas_age_{0}.mhd".format(ages[ageidx+1]))

    match_cfg.io.outputPrefix = os.path.join(outdir, "atlas_match_age{0}_age{1}_".format(ages[ageidx], ages[ageidx+1]))
      
    vmMatching.Matching(match_cfg)


if __name__ == '__main__':
  # actually going to call this code for each group, since stored in different files.
  
  usage = """
                %prog [options] <input atlas directory> <output atlas matching directory>

Computes imaging from each age atlas to the next one in range [beginage, endage, step]"""

  optparser = OptionParser(usage=usage)
  optparser.add_option("-b", "--beginage", dest="begin_age",
                       type = "int", default=70, 
                       help="beginning age for analysis")
  optparser.add_option("-e","--endage", dest="end_age",
                       type = "int", default=85,
                       help="end age for analysis")
  optparser.add_option("-s","--step", dest="age_step",
                       type = "int", default=1,
                       help="size of step between ages in years")
  (options, args) = optparser.parse_args()

  num_expected_args = 2
  if len(args) != num_expected_args:
      optparser.error("Incorrect number of arguments, %d provided, %d expected." % (len(args),
                                                                                    num_expected_args))

  exe_dir, exe = os.path.split(sys.argv[0])
  atlas_dir = args[0]
  match_dir = args[1]

  age_range = range(options.begin_age, options.end_age+1, options.age_step)

  if len(age_range) > 1:
    time_range = [ (age - options.begin_age) / float(options.end_age - options.begin_age) for age in age_range ]

    do_matching(age_range, time_range, atlas_dir, match_dir)

