#!/usr/bin/python2
import sys
import os.path
from optparse import OptionParser
import csv

# pyca
import PyCA.Core as ca
import PyCA.Common as common

# vector momentum configs
from Configs import Config

# vector momentum modules
from Applications import CAvmGeodesicShooting as vmShooting

subject_dir = "/usr/sci/projects/ADNI/nikhil/HGM/braindata/OASISdata/Individual/3D/Results"

def get_subject_filenames(subj_id):
  ifname = subj_id + "I0.mhd"
  mfname = subj_id + "m0.mhd"
  return(os.path.join(subject_dir, ifname), 
         os.path.join(subject_dir, mfname))

# read data in from csv
def parse_csv(fname, age_range):
  csvfile = open(fname, 'rb')
  reader = csv.reader(csvfile, delimiter=",", quotechar='"')

  subjs = {}
  ages = {}
  header = []
  rowcnt = 0
  for row in reader:
    if rowcnt == 0:
      for label in row:
        header.append(label.strip())
    else:
      if len(row) == len(header):
        # hard-coding knowledge of which column is which
        subj = row[1].strip()
        age = int(row[2].strip())
        timept = float(row[3].strip())
        weight = float(row[4].strip())
        
        if age in age_range:
          # Set up subjects
          if not subjs.has_key(subj):
            subjs[subj] = []
          if timept not in subjs[subj]:
            subjs[subj].append(timept)

          # Set up age bins
          if not ages.has_key(age):
            ages[age] = []
          ages[age].append({"subj": subj, "time": timept, "weight": weight})

    rowcnt += 1

  return subjs, ages


# for each individual, make prediction by shooting along the geodesic, saving result in an age folder
def predict_subjs(subjs, subj_dir):
  print("Processing subjects: ", subjs)
  shoot_cfg = Config.SpecToConfig(vmShooting.GeodesicShootingConfigSpec)
  shoot_cfg.io.saveFrames = False
  shoot_cfg.io.doPlot = False

  for subj in subjs:
    ifname, mfname = get_subject_filenames(subj)
    shoot_cfg.study.I0 = ifname
    shoot_cfg.study.m0 = mfname
    for tp in subjs[subj]:
      shoot_cfg.study.scaleMomenta = tp
      shoot_cfg.io.outputPrefix = os.path.join(subj_dir, "{0}_{1}_".format(subj, tp))
      
      vmShooting.GeodesicShooting(shoot_cfg)
                

# for each age point, do a weighted average of the predictions, and save off
def age_average(ages, subj_dir, atlas_dir):
  print("Processing ages: ", ages)
  # mem type is determined by whether or not we're using CUDA
  #mType = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST
  mType = ca.MEM_HOST

  for age in ages:
    do_init_atlas = True
    atlas = None 
    weight_sum = 0.0
    for img in ages[age]:
      imgfile = os.path.join(subj_dir, 
                             "{0}_{1}_I1.mhd".format(img["subj"], img["time"]))
      curimg = common.LoadITKImage(imgfile, mType)
      # scale image by weight and add to total weight
      ca.MulC_I(curimg, img["weight"])
      weight_sum += img["weight"]

      if do_init_atlas:
        atlas = ca.Image3D(curimg.grid(), mType)
        ca.SetMem(atlas, 0.0)
        do_init_atlas = False

      ca.Add_I(atlas, curimg)

    # scale and save off atlas
    if weight_sum > 0.0:
      ca.MulC_I(atlas, 1.0 / weight_sum)

    atlas_file = os.path.join(atlas_dir, 
                              "atlas_age_{0}.mhd".format(age))
    common.SaveITKImage(atlas, atlas_file)

if __name__ == '__main__':
  # actually going to call this code for each group, since stored in different files.
  
  usage = """
                %prog [options] <input csv file> <output subject directory> <output atlas directory>

Computes weighted average of individuals at each age in range [beginage, endage, step]"""

  optparser = OptionParser(usage=usage)
  optparser.add_option("-a", "--atlasonly", dest="do_atlas_only",
                       action="store_true", default=False, 
                       help="compute age atlas only, skip interpolation of individuals.  This assumes the interpolation has already been done.")
  optparser.add_option("-b", "--beginage", dest="begin_age",
                       type = "int", default=70, 
                       help="beginning age for analysis")
  optparser.add_option("-e","--endage", dest="end_age",
                       type = "int", default=85,
                       help="end age for analysis")
  optparser.add_option("-s","--step", dest="age_step",
                       type = "int", default=1,
                       help="size of step between ages in years")
  (options, args) = optparser.parse_args()

  num_expected_args = 3
  if len(args) != num_expected_args:
      optparser.error("Incorrect number of arguments, %d provided, %d expected." % (len(args),
                                                                                    num_expected_args))

  exe_dir, exe = os.path.split(sys.argv[0])
  fname = args[0]
  subj_dir = args[1]
  atlas_dir = args[2]

  age_range = range(options.begin_age, options.end_age+1, options.age_step)

  subjs, ages = parse_csv(fname, age_range)
  
  if not options.do_atlas_only:
    predict_subjs(subjs, subj_dir)

  age_average(ages, subj_dir, atlas_dir)
