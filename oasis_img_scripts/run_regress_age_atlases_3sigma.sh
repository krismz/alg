#!/bin/bash

MPIDIR=/opt/openmpi
export PATH=${PATH}:/usr/local/bin:${MPIDIR}/bin

export PYTHONPATH=${PYTHONPATH}:/home/sci/kris/Software/vectormomentum/Code/Python/
export PYTHONPATH=/home/sci/kris/suse_13.2_bin/python2.7/site-packages/:${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:/usr/lib64/python2.6/site-packages/openmpi

PROGNAME=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/scripts/geodesic_regression_age_atlases.py

DEMATLASDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Demented
NONDEMATLASDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Nondemented
CONVATLASDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Converted

RESULTSDIR=Regression_65_90_by_5/

mkdir -p $DEMATLASDIR/$RESULTSDIR
mkdir -p $NONDEMATLASDIR/$RESULTSDIR
mkdir -p $CONVATLASDIR/$RESULTSDIR

echo "================ start time ================"
date
echo "============================================"

#python2 $PROGNAME -b 65 -e 90 -s 5 $DEMATLASDIR $DEMATLASDIR/$RESULTSDIR
python2 $PROGNAME -b 65 -e 90 -s 5 $NONDEMATLASDIR $NONDEMATLASDIR/$RESULTSDIR
#python2 $PROGNAME -b 65 -e 90 -s 5 $CONVATLASDIR $CONVATLASDIR/$RESULTSDIR

echo "================ end time ================"
date
echo "============================================"
