#!/bin/bash
# The name of the job:
#PBS -N CAvmAtlas
# How long to allow the job to run:
#PBS -l walltime=12:00:00
# The rest can just be left as is:
#PBS -q batch
#PBS -j oe
#PBS -k o
#PBS -m ae

MPIDIR=/opt/openmpi
export PATH=${PATH}:/usr/local/bin:${MPIDIR}/bin
#export PYTHONPATH=${PYTHONPATH}:/home/sci/nsingh/python/cluster/site-packages/:/usr/sci/projects/ADNI/nikhil/software/vectormomentum/Code/Python/
export PYTHONPATH=${PYTHONPATH}:/home/sci/kris/Software/vectormomentum/Code/Python/
export PYTHONPATH=/home/sci/kris/suse_13.2_bin/python2.7/site-packages/:${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:/usr/lib64/python2.6/site-packages/openmpi

PROGNAME=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/scripts/predict_individuals_at_age.py
PROGARGS=vm_build_atlas.yaml
TEMPLATECONFIGPATH=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/templates/
#RESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/test_vector_momenta/Demented_3subj
DEMSUBJRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/subj_results/Demented
NONDEMSUBJRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/subj_results/Nondemented
CONVSUBJRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/subj_results/Converted
DEMATLASRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Demented
NONDEMATLASRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Nondemented
CONVATLASRESULTSDIR=/usr/sci/projects/Autism/krismz/longitudinal_oasis_analysis/3sigma_atlas/Converted
#cd ${PBS_O_WORKDIR}


mkdir -p $DEMSUBJRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${DEMSUBJRESULTSDIR}/
mkdir -p $NONDEMSUBJRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${NONDEMSUBJRESULTSDIR}/
mkdir -p $CONVSUBJRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${CONVSUBJRESULTSDIR}/

mkdir -p $DEMATLASRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${DEMATLASRESULTSDIR}/
mkdir -p $NONDEMATLASRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${NONDEMATLASRESULTSDIR}/
mkdir -p $CONVATLASRESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${CONVATLASRESULTSDIR}/


#cat ${PBS_NODEFILE}|sort|uniq>NODESLIST
#cat ${PBS_NODEFILE}|sort>NODESLIST
#echo "================ env ================"
#env | sort
#echo "====================================="
#echo "================ nodes ================"
#cat NODESLIST
#echo "======================================="
echo "================ start time ================"
date
echo "============================================"

#${MPIDIR}/bin/mpirun \
#    -x PYTHONPATH \
#    --machinefile NODESLIST -np ${NP}\
#    python2 $PROGNAME $PROGARGS

#python2 $PROGNAME -b 60 -e 95 -s 1 -a $TEMPLATECONFIGPATH/ages_to_regress_6sigma_Demented_3subj.csv $RESULTSDIR
python2 $PROGNAME -b 60 -e 95 -s 1 -a $TEMPLATECONFIGPATH/ages_to_regress_3sigma_Demented.csv $DEMSUBJRESULTSDIR $DEMATLASRESULTSDIR
python2 $PROGNAME -b 60 -e 95 -s 1 -a $TEMPLATECONFIGPATH/ages_to_regress_3sigma_Nondemented.csv $NONDEMSUBJRESULTSDIR $NONDEMATLASRESULTSDIR
python2 $PROGNAME -b 60 -e 95 -s 1 -a $TEMPLATECONFIGPATH/ages_to_regress_3sigma_Converted.csv $CONVSUBJRESULTSDIR $CONVATLASRESULTSDIR

# copy output to current directory
#cp ~/${PBS_JOBNAME}.o${PBS_JOBID%%.*} .
 
echo "================ end time ================"
date
echo "============================================"
