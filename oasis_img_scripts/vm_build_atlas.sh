#!/bin/bash
# The name of the job:
#PBS -N CAvmAtlas
# How long to allow the job to run:
#PBS -l walltime=12:00:00
# The rest can just be left as is:
#PBS -q batch
#PBS -j oe
#PBS -k o
#PBS -m ae

MPIDIR=/opt/openmpi
export PATH=${PATH}:/usr/local/bin:${MPIDIR}/bin
#export PYTHONPATH=${PYTHONPATH}:/home/sci/nsingh/python/cluster/site-packages/:/usr/sci/projects/ADNI/nikhil/software/vectormomentum/Code/Python/
export PYTHONPATH=${PYTHONPATH}:/home/sci/kris/Software/vectormomentum/Code/Python/
export PYTHONPATH=${PYTHONPATH}:/usr/lib64/python2.6/site-packages/openmpi

PROGNAME=/home/sci/kris/Software/vectormomentum/Code/Python/Applications/CAvmAtlas.py
PROGARGS=vm_build_atlas.yaml
TEMPLATECONFIGPATH=/usr/sci/projects/autism/krismz/longitudinal_oasis_analysis/templates/
RESULTSDIR=/usr/sci/projects/autism/krismz/longitudinal_oasis_analysis/test_vector_momenta_results/
#cd ${PBS_O_WORKDIR}

mkdir -p $RESULTSDIR
cp -r ${TEMPLATECONFIGPATH}/* ${RESULTSDIR}/

cd ${RESULTSDIR}

#cat ${PBS_NODEFILE}|sort|uniq>NODESLIST
#cat ${PBS_NODEFILE}|sort>NODESLIST
#echo "================ env ================"
#env | sort
#echo "====================================="
#echo "================ nodes ================"
#cat NODESLIST
#echo "======================================="
echo "================ start time ================"
date
echo "============================================"

#${MPIDIR}/bin/mpirun \
#    -x PYTHONPATH \
#    --machinefile NODESLIST -np ${NP}\
#    python2 $PROGNAME $PROGARGS

python2 $PROGNAME $PROGARGS

# copy output to current directory
#cp ~/${PBS_JOBNAME}.o${PBS_JOBID%%.*} .
 
