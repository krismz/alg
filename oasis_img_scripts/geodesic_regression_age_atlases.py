#!/usr/bin/python2
import sys
import os.path
from optparse import OptionParser
import csv

# pyca
import PyCA.Core as ca
import PyCA.Common as common

# vector momentum configs
from Configs import Config

# vector momentum modules
from Applications import CAvmGeodesicRegression as vmGeoReg
from Applications import CAvmGeodesicShooting as vmShooting

def normalize_imgs(imgs):
  for img in imgs:
    maxval = ca.Max(img)
    print("img max: ", maxval)
    ca.MulC_I(img, 1.0 / maxval)


def subj_details(ages, indir, cfg):
  # TODO this is very specific to my ageatlas experiment, change to config params instead?
  subj_id = "atlas"
  imgs = []
  for a in ages:
    imgs.append(os.path.join(indir, "{0}_age_{1}.mhd".format(subj_id, a)))
  
  return (subj_id, imgs)  

def do_BuildGeoReg(ages, times, indir, cf):
# call this instead of vmGeoReg.BuildGeoReg
  common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))
    
  (subjid, imgpaths) = subj_details(ages, indir, cf)

  cfstr = Config.ConfigToYAML(vmGeoReg.GeoRegConfigSpec, cf)  
  with open(cf.io.outputPrefix + "georegconfig.yaml", "w") as f:
    f.write(cfstr)

  if cf.compute.useMPI:
    raise Exception("MPI probably not supported correctly yet")

  # mem type is determined by whether or not we're using CUDA
  mType = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST    

  # get image size information
  dummyImToGetGridInfo = common.LoadITKImage(imgpaths[0], mType)
  imGrid = dummyImToGetGridInfo.grid();
  if cf.study.setUnitSpacing:
    imGrid.setSpacing(ca.Vec3Df(1.0, 1.0, 1.0))
  if cf.study.setZeroOrigin:
    imGrid.setOrigin(ca.Vec3Df(0, 0, 0))
  #del dummyImToGetGridInfo;
    
  # start up the memory manager for scratch variables
  ca.ThreadMemoryManager.init(imGrid, mType, 0)

  # allocate memory     
  p = vmGeoReg.GeoRegVariables(imGrid, mType, cf.vectormomentum.diffOpParams[0], cf.vectormomentum.diffOpParams[1], cf.vectormomentum.diffOpParams[2], cf.optim.NIterForInverse, cf.vectormomentum.sigma, cf.optim.stepSize,  integMethod=cf.optim.integMethod)

  # initializations for this subject
  ca.SetMem(p.m0,0.0)
  ca.SetMem(p.I0,0.0)
        
  # allocate memory specific to this subject in steps a, b and c        
  # a. create time array with checkpointing info for regression geodesic, allocate checkpoint memory
  (t, msmtinds, cpinds) = vmGeoReg.GeoRegSetUpTimeArray(cf.optim.nTimeSteps, times, 0.005)
  # we have enough measurements do only do timepoints at each measurement, so set these manually instead
  t = times
  cpinds = range(1,len(t))
  msmtinds = [ m-1 for m in range(len(cpinds)+1) ] #include last time point

  print ("times: ", times)
  print("time array output, t: ", t, ", msmtinds: ", msmtinds, ", cpinds: ", cpinds)

  cpstates =  [(ca.Field3D(imGrid,mType),ca.Field3D(imGrid,mType)) for idx in cpinds]

  # b. allocate gradAtMeasurements of the length of msmtindex for storing residuals
  gradAtMsmts =  [ca.Image3D(imGrid,mType) for idx in msmtinds]        
        
  # c. load timepoint images for this subject
  Imsmts = [common.LoadITKImage(f, mType) for f in imgpaths]
  # reset stepsize if adaptive stepsize changed it inside
  p.stepSize = cf.optim.stepSize

  # preprocess images
  # skipping because atlases are already between 0 and 1
  #GeoRegPreprocessInput(subjid, cf, p, t,Imsmts, cpinds, cpstates, msmtinds, gradAtMsmts)
  # but do need to intensity normalize so full range of 0 to 1, not just 0 to .67 or whatever
  normalize_imgs(Imsmts)

  # run regression for this subject
  # REMEMBER
  # msmtinds index into cpinds
  # gradAtMsmts is parallel to msmtinds
  # cpinds index into t
  EnergyHistory = vmGeoReg.RunGeoReg(subjid, cf, p, t, Imsmts, cpinds, cpstates, msmtinds, gradAtMsmts)

  # write output images and fields for this subject
  # TODO: BEWARE There are hardcoded numbers inside preprocessing code specific for ADNI/OASIS brain data.
  vmGeoReg.GeoRegWriteOuput(subjid, cf, p, t, Imsmts, cpinds, cpstates, msmtinds, gradAtMsmts, EnergyHistory)
  # clean up memory specific to this subject
  del t,Imsmts, cpinds, cpstates, msmtinds, gradAtMsmts
        
    
# end do_BuildGeoReg

def do_regression(ages, times, indir, outdir):
# do a regression of all atlases in the ages and put the results in outdir

  # setup config, matching nikhil's HGM georeg.yaml params
  reg_cfg = Config.SpecToConfig(vmGeoReg.GeoRegConfigSpec)
  reg_cfg.study.numSubjects = 1
  reg_cfg.study.initializationsFile = None
  reg_cfg.io.outputPrefix = outdir
  reg_cfg.compute.useCUDA = False # True?
  reg_cfg.compute.interactive = False # True?
  reg_cfg.optim.nTimeSteps = len(ages) # 1 time step per measurement
  reg_cfg.optim.Niter = 30 # Nikhil used 2000, but convergence seems to happen around 30
  reg_cfg.optim.stepSize = 0.0005
  reg_cfg.optim.integMethod = "RK4"
  reg_cfg.vectormomentum.sigma = 0.1

  # TODO wrap in Compute.Compute call to do MPI spawns etc
  do_BuildGeoReg(ages, times, indir, reg_cfg)

# end do_regression

def do_prediction(ages, times, outdir):
# predict atlas at each age by shooting according to m0 found from regression
 
  # setup config
  shoot_cfg = Config.SpecToConfig(vmShooting.GeodesicShootingConfigSpec)
  shoot_cfg.io.saveFrames = False
  shoot_cfg.io.doPlot = False

  shoot_cfg.study.I0 = outdir + "atlasI0.mhd"
  shoot_cfg.study.m0 = outdir + "atlasm0.mhd"

  for age, tp in zip(ages, times):
    shoot_cfg.study.scaleMomenta = tp
    shoot_cfg.io.outputPrefix = os.path.join(outdir, "atlas_age{0}_{1}_".format(age, tp))
      
    vmShooting.GeodesicShooting(shoot_cfg)


if __name__ == '__main__':
  # actually going to call this code for each group, since stored in different files.
  
  usage = """
                %prog [options] <input atlas directory> <output atlas regression directory>

Computes geodesic regression from each age atlas in range [beginage, endage, step]"""

  optparser = OptionParser(usage=usage)
  optparser.add_option("-p", "--predictonly", dest="do_predict_only",
                       action="store_true", default=False,
                       help="predict atlas for each age point based on shooting only.  Skip finding the geodesic regression.  This assumes that the regression has already been done")
  optparser.add_option("-b", "--beginage", dest="begin_age",
                       type = "int", default=70, 
                       help="beginning age for analysis")
  optparser.add_option("-e","--endage", dest="end_age",
                       type = "int", default=85,
                       help="end age for analysis")
  optparser.add_option("-s","--step", dest="age_step",
                       type = "int", default=1,
                       help="size of step between ages in years")
  (options, args) = optparser.parse_args()

  num_expected_args = 2
  if len(args) != num_expected_args:
      optparser.error("Incorrect number of arguments, %d provided, %d expected." % (len(args),
                                                                                    num_expected_args))

  exe_dir, exe = os.path.split(sys.argv[0])
  atlas_dir = args[0]
  regress_dir = args[1]

  age_range = range(options.begin_age, options.end_age+1, options.age_step)

  if len(age_range) > 1:
    time_range = [ (age - options.begin_age) / float(options.end_age - options.begin_age) for age in age_range ]

    if not options.do_predict_only:
      do_regression(age_range, time_range, atlas_dir, regress_dir)

    do_prediction(age_range, time_range, regress_dir)
