from optparse import OptionParser
import sys
import csv
import os.path

def parse_csv(incsv):
  csvfile = open(incsv, 'rU')
  reader = csv.reader(csvfile, delimiter=",")

  subjs = {}
  header = []
  rowcnt = 0
  ageyrs_cols = [9, 10, 11, 12]
  for row in reader:
    if rowcnt == 0:
      for label in row:
        header.append(label.strip())

    else:
      if len(row) == len(header):
        subj = 'AD' + row[0].strip()
        ages = []
        times = []
        minage=200
        maxage=0
        for col in ageyrs_cols:
          if row[col].strip():
            val = int(row[col].strip())
            ages.append(val)
            if val > maxage:
              maxage = val
            if val < minage:
              minage = val
          else:
            ages.append(-1)
        if maxage != minage:    
          times = [ (age - minage) / float(maxage-minage) for age in ages ]
          subjs[subj] = {"Ages": ages, "Times": times}

    rowcnt += 1
  return subjs

def make_subj_tp_filename(subj, idx):
  basedir = '/usr/sci/projects/Autism/preprocessed_data'
  subjdir = os.path.join(basedir, subj)
  tpdir = os.path.join(subjdir, '{0}'.format(idx))
  filename = os.path.join(tpdir,
                          "{0}_{1}_t1_stripped_irescaled.nhdr".format(subj,idx))
  return filename

def write_txt(subjs, outtxt):
  outfile = open(outtxt, 'w')
  writer = csv.writer(outfile, delimiter=',')
  for subj in subjs:
    writer.writerow([subj])
    ages = subjs[subj]["Ages"]
    times = subjs[subj]["Times"]
    files = []
    valid_times = []
    for idx in range(len(ages)):
      if ages[idx] > 0:
        files.append(make_subj_tp_filename(subj, idx+1))
        valid_times.append(times[idx])
    writer.writerow(files)
    writer.writerow(valid_times)

def convert_to_subj_details(incsv, outtxt):
  subjs = parse_csv(incsv)
  write_txt(subjs, outtxt)

if __name__ == "__main__":
  usage = """                                                           
         %prog [options] <input subject csv> <output details txt> 
                                                                       
Convert age in years into time points"""  
                                                                       
  optparser = OptionParser(usage=usage)
  (options, args) = optparser.parse_args()
                                                                       
  num_expected_args = 2                             
  if len(args) != num_expected_args:
    optparser.error("Incorrect number of arguments, %d provided, %d expected." % (len(args),
                                                              num_expected_args)) 
                                                                        
  exe_dir, exe = os.path.split(sys.argv[0])
  subj_csv = args[0]
  out_txt = args[1]

  convert_to_subj_details(subj_csv, out_txt)

